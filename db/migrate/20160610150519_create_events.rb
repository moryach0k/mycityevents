class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :city
      t.text :themes
      t.datetime :start_date_time
      t.datetime :finish_date_time

      t.timestamps null: false
    end
  end
end
