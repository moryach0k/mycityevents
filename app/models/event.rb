class Event < ActiveRecord::Base
  validates :name, :city, :themes, :start_date_time, :finish_date_time, presence: true
  validate :valid_date_time

  scope :filtered_by_city, -> (city) { where(city: city) }
  scope :filtered_by_themes, -> (themes) { where("themes like ?", "%#{themes}%") }
  scope :filtered_by_date_time, -> (start_date_time, finish_date_time) { where("start_date_time <= ? AND finish_date_time >= ?", start_date_time, finish_date_time) }


  private
  def valid_date_time
    if start_date_time > finish_date_time
      errors.add(:start_date_time, "The start date and time can't be higher than finish date and time")
      errors.add(:finish_date_time, "The finish date time can't be lower than start date time")
    end
  end
end
