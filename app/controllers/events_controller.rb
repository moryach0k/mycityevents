class EventsController < ApplicationController
  def index
    @search = Event.ransack(params[:q])
    @events = @search.result
  end

  def show
    @event = Event.find(params[:id])
  end

  def create
    @event = Event.new(event_params)
    @event.save
  end

  def update
    @event = Event.find(params[:id])
    @event.update(event_params)
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
  end

  private
  def event_params
    params.require(:event).permit(:name, :city, :themes, :start_date_time, :finish_date_time)
  end
end
